import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import es from 'vuetify/src/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#cddc39",
        secondary: "#009688",
        accent: "#ffc107",
        error: "#f44336",
        warning: "#ff9800",
        info: "#607d8b",
        success: "#03a9f4"
      },
    },
  },
    lang: {
      locales: { es },
      current: 'es',
    },
});

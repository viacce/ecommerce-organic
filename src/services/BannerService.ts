class BannerService {

    //TODO: Refactor to call webservices and replace this mock implementation.
    static getLandingBanners = () => {
        return new Promise((resolve, reject) => {
            const banners = require('./mock/banners.json')
            const object = banners.find(
                (object: { page: string }) => object.page === 'Landing'
            )
            resolve(object.images)
        })
    }
}

export default BannerService

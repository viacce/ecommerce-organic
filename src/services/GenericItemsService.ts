class GenericItemsService {

    //TODO: Refactor to call webservices and replace this mock implementation.
    static getListOfItemsById = (_id: string) => {
        return new Promise((resolve, reject) => {
            const genericItems = require('./mock/generic-items.json')
            const object = genericItems.find(
                (object: { id: string }) => object.id === _id
            )
            resolve(object.items)
        })
    }
}

export default GenericItemsService

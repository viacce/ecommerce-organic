class SocialNetworksService {

    //TODO: Refactor to call webservices and replace this mock implementation.
    static getSocialNetworks = () => {
        return new Promise((resolve, reject) => {
            const socialNetworks = require('./mock/social-networks.json')
            resolve(socialNetworks)
        })
    }
}

export default SocialNetworksService
